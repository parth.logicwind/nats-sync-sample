const NATS = require('nats')
const servers = ['nats://68.183.87.233:30513']

const natsConnect = NATS.connect({ servers, json: true })

natsConnect.publish('cb-sync-queue', "Hello World")
console.log('Message Published')

return true
