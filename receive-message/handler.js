module.exports = (event, context) => {
  console.log('-----START------')
  console.log({ event })
  const result = {
    status: "You said: " + JSON.stringify(event.body)
  };
  console.log('-----END------')
  context
    .status(200)
    .succeed(result);
}
